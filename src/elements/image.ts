import { Box } from './box';
import jsPDF from 'jspdf';
import { AbstractElement } from './abstract-element';
import { IContext } from './context';
import { GenerateTypeEnum } from './generate-type.enum';

export class Image extends Box {
  public imageData: string;

  constructor(
    globalType: GenerateTypeEnum,
    x: number,
    y: number,
    w: number,
    h: number,
    imageData: string,
    context: Partial<IContext> = AbstractElement.DEFAULT_CONTEXT
  ) {
    super(globalType, x, y, w, h, context);
    this.imageData = imageData;
  }

  public render(doc: jsPDF, _maxWidth?: number): jsPDF {
    doc.addImage({
      imageData: this.imageData,
      x: this.x,
      y: this.y,
      width: this.w,
      height: this.h,
    });
    return doc;
  }

  public renderHtml(
    doc: Document,
    parent: HTMLElement,
    cssRules: string[],
    sheet: HTMLStyleElement
  ): Document {
    const img = doc.createElement('img');
    img.src = this.imageData;
    const css = `img-${this.w ?? 0}`;
    img.classList.add(`img`);
    img.classList.add(css);
    img.classList.add(this.context.name);
    if (!cssRules.includes(css)) {
      cssRules.push(css);
      let rule = '';
      if (this.w > 0) {
        rule += `width: ${this.w}px;`;
      }
      if (rule.length > 0) {
        sheet.innerHTML += ` .${css} { ${rule} }`;
      }
    }
    parent.append(img);
    return doc;
  }

  public getElements(): AbstractElement[] {
    return [this];
  }
}
