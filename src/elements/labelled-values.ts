import { Row } from './row';
import { Column } from './column';
import { LabelledValue } from './labelled-value';
import jsPDF from 'jspdf';
import { MARGINS } from '../constants';
import { IContext } from './context';
import { AbstractElement } from './abstract-element';
import { GenerateTypeEnum } from './generate-type.enum';

export class LabelledValues extends Row {
  public labelledValues: { label: string; value: number }[];
  public nbrOfCol: number;

  constructor(
    globalType: GenerateTypeEnum,
    x: number,
    y: number,
    labelledValues: { label: string; value: number }[],
    nbrOfCol?: number,
    multiline = false,
    context: Partial<IContext> = AbstractElement.DEFAULT_CONTEXT
  ) {
    super(globalType, x, y, [], undefined, undefined, undefined, context);
    this.labelledValues = labelledValues;
    this.nbrOfCol = nbrOfCol ?? 3;
    if (this.nbrOfCol > 3) {
      this.nbrOfCol = 3;
    }
    const valuePercent = 5 * this.nbrOfCol;
    const labelPercent = 100 - valuePercent;
    const widthPercent = [labelPercent, valuePercent];
    let currentIndex = 0;
    if (labelledValues.length > 0) {
      if (this.nbrOfCol > 1) {
        const nbrPerCol = Math.floor(labelledValues.length / this.nbrOfCol);
        const rest = labelledValues.length - nbrPerCol * this.nbrOfCol;
        const nbrPerCols = [
          rest > 0 ? nbrPerCol + 1 : nbrPerCol,
          rest > 1 ? nbrPerCol + 1 : nbrPerCol,
          rest > 2 ? nbrPerCol + 1 : nbrPerCol,
        ];
        for (let i = 0; i < this.nbrOfCol; i++) {
          this.contextElements[i] = new Column(globalType, 0, 0, [], {
            name: `${this.context.name}-column`,
          });
        }
        for (let i = 0; i < labelledValues.length; i++) {
          if (i < nbrPerCols[0]) {
            currentIndex = 0;
          } else if (i < nbrPerCols[0] + nbrPerCols[1]) {
            currentIndex = 1;
          } else {
            currentIndex = 2;
          }
          (<Column>this.contextElements[currentIndex]).contextElements.push(
            new LabelledValue(
              globalType,
              labelledValues[i].label,
              labelledValues[i].value,
              widthPercent,
              multiline
            )
          );
        }
      } else {
        this.contextElements.push(
          new Column(
            globalType,
            0,
            0,
            labelledValues.map(
              (libelledValue) =>
                new LabelledValue(
                  globalType,
                  libelledValue.label,
                  libelledValue.value,
                  widthPercent,
                  multiline
                )
            ),
            {
              name: `${this.context.name}-column`,
            }
          )
        );
      }
    }
  }

  public prepareRender(doc: jsPDF, maxWidth?: number): jsPDF {
    const pageWidth = doc.internal.pageSize.width;
    const rowWidth = pageWidth - this.x - MARGINS.right;
    for (const column of this.pdfElements) {
      for (const labelledValue of (<Column>column).pdfElements) {
        labelledValue.maxWidth = rowWidth / this.nbrOfCol;
      }
    }
    return super.prepareRender(doc, maxWidth);
  }

  public render(doc: jsPDF, _maxWidth?: number): jsPDF {
    return doc;
  }
}
