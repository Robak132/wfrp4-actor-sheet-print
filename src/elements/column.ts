import { AbstractElement } from './abstract-element';
import jsPDF from 'jspdf';
import { IContext } from './context';
import { AbstractContainerElement } from './abstract-container-element';
import { GenerateTypeEnum } from './generate-type.enum';

export class Column extends AbstractContainerElement {
  constructor(
    globalType: GenerateTypeEnum,
    x: number,
    y: number,
    elements: AbstractElement[],
    context: Partial<IContext> = AbstractElement.DEFAULT_CONTEXT
  ) {
    super(globalType, x, y, undefined, elements, context);
  }

  public prepareRender(doc: jsPDF, _maxWidth?: number): jsPDF {
    const elements = this.pdfElements ?? [];

    let currentY = this.y;
    for (let i = 0; i < elements.length; i++) {
      const element = elements[i];
      element.x = Math.max(element.x, this.x);
      element.y = currentY;
      element.prepareRender(doc);
      const elementHeight = element.getHeight(doc);
      currentY +=
        elementHeight > 0 ? element.getHeight(doc) + 2 : elementHeight;
    }
    return doc;
  }

  public render(doc: jsPDF, _maxWidth?: number): jsPDF {
    return doc;
  }

  public renderHtml(
    doc: Document,
    parent: HTMLElement,
    cssRules: string[],
    sheet: HTMLStyleElement
  ): Document {
    const div = doc.createElement('div');
    div.classList.add(`column`);
    div.classList.add(this.context.name);
    const elements = this.htmlElements ?? [];
    for (let i = 0; i < elements.length; i++) {
      const element = elements[i];
      element.renderHtml(doc, div, cssRules, sheet);
    }
    parent.append(div);
    return doc;
  }

  public getHeight(doc): number {
    return this.pdfElements.length > 0
      ? this.pdfElements
          .map((e) => e.getHeight(doc))
          .reduce((p, c, i) => {
            if (i === 0) {
              return c;
            }
            return p + c + 2;
          })
      : 0;
  }

  public getCheckNewPageHeight(doc?: jsPDF): number {
    return this.pdfElements.length > 0
      ? this.pdfElements[0].getCheckNewPageHeight(doc)
      : 0;
  }
}
