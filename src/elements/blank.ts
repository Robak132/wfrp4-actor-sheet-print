import { AbstractElement } from './abstract-element';
import jsPDF from 'jspdf';
import { Box } from './box';
import { IContext } from './context';
import { GenerateTypeEnum } from './generate-type.enum';

export class Blank extends Box {
  constructor(
    globalType: GenerateTypeEnum,
    x: number,
    y: number,
    w: number,
    h: number,
    context: Partial<IContext> = AbstractElement.DEFAULT_CONTEXT
  ) {
    super(globalType, x, y, w, h, context);
  }

  public static heightBlank(globalType: GenerateTypeEnum, h: number) {
    return new Blank(globalType, 0, 0, 0, h);
  }

  public getCheckNewPageHeight(doc?: jsPDF): number {
    return this.getHeight(doc);
  }

  public prepareRender(doc: jsPDF, _maxWidth?: number): jsPDF {
    return doc;
  }

  public render(doc: jsPDF, _maxWidth?: number): jsPDF {
    return doc;
  }

  public getElements(): AbstractElement[] {
    return [this];
  }
}
