import { Row } from './row';
import { Text } from './text';
import { MultilineText } from './multiline-text';
import { IContext } from './context';
import { AbstractElement } from './abstract-element';
import { GenerateTypeEnum } from './generate-type.enum';

export class LabelledValue extends Row {
  public label: string;
  public value: number;

  constructor(
    globalType: GenerateTypeEnum,
    label: string,
    value: number,
    widthPercents?: number[],
    multiline = false,
    maxWidth?: number,
    context: Partial<IContext> = AbstractElement.DEFAULT_CONTEXT
  ) {
    super(
      globalType,
      0,
      0,
      [
        multiline
          ? new MultilineText(globalType, 0, 0, label)
          : new Text(globalType, 0, 0, label),
        new Text(globalType, 0, 0, value.toString(), {
          align: 'right',
        }),
      ],
      maxWidth,
      widthPercents,
      [],
      context
    );
    this.label = label;
    this.value = value;
  }

  public renderHtml(
    doc: Document,
    parent: HTMLElement,
    cssRules: string[],
    sheet: HTMLStyleElement
  ): Document {
    const resultDoc = super.renderHtml(doc, parent, cssRules, sheet);
    parent.lastElementChild?.classList?.add('labelled-value');
    return resultDoc;
  }
}
