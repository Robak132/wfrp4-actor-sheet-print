export interface IContext {
  name: string;
  isHtml: boolean;
  isPdf: boolean;
}
