import { PdfBuilder } from './pdf-builder';
import { LabelledText } from './elements/labelled-text';
import { Row } from './elements/row';
import { Image } from './elements/image';
import { Box } from './elements/box';
import { Util } from './util';
import { i18nLocalize, isGM, MARGINS } from './constants';
import { ItemData } from '@league-of-foundry-developers/foundry-vtt-types/src/foundry/common/data/data.mjs';
import { LabelledValues } from './elements/labelled-values';
import { Text } from './elements/text';
import { Texts } from './elements/texts';
import { Column } from './elements/column';
import { Separator } from './elements/separator';
import { Blank } from './elements/blank';
import { AbstractBuilder } from './abstract-builder';
import { HtmlBuilder } from './html-builder';

Hooks.on('getActorDirectoryEntryContext', async (_, options) => {
  options.push(
    {
      name: i18nLocalize('WFRP4SHEETPRINT.export.pdf'),
      condition: isGM(),
      icon: '<i class="fas fa-file-pdf"></i>',
      callback: async (target) => {
        const actor: Actor & any = (<any>game).actors.get(
          target.attr('data-document-id')
        );
        const docBuilder = new PdfBuilder({
          orientation: 'p',
          unit: 'mm',
        });
        await generate(actor, docBuilder);
      },
    },
    {
      name: i18nLocalize('WFRP4SHEETPRINT.export.html'),
      condition: isGM(),
      icon: '<i class="fas fa-file-code"></i>',
      callback: async (target) => {
        const actor: Actor & any = (<any>game).actors.get(
          target.attr('data-document-id')
        );
        const docBuilder = new HtmlBuilder();
        await generate(actor, docBuilder);
      },
    }
  );
});

Hooks.on(
  'renderActorSheetWfrp4eCharacter',
  async (app: ActorSheet, html: JQuery) => {
    const actor: Actor & any = app.actor;

    addActorSheetActionButton(html, 'print', async () => {
      const docBuilder = new PdfBuilder({
        orientation: 'p',
        unit: 'mm',
      });
      await generate(actor, docBuilder);
    });
  }
);

function addActorSheetActionButton(
  html: JQuery,
  icon: string,
  onClick: () => void
) {
  const button = document.createElement('a');
  button.classList.add('print');
  button.innerHTML = `<i class="fas fa-${icon}"> </i>`;
  button.addEventListener('click', () => {
    onClick();
  });
  const header = html.find('.window-header');
  const title = header.find('.window-title');
  title.after(button);
}

async function generate(actor: Actor & any, docBuilder: AbstractBuilder) {
  const actorData = actor;
  // @ts-ignore
  const actorDetails = actorData.system.details;
  const actorStatus = actorData.system.status;
  const actorCharacs = actor.characteristics;
  const actorImage = actor.img;
  let actorImageData: string | null = null;
  if (actorImage != null) {
    const texture = await loadTexture(actorImage);
    actorImageData = ImageHelper.textureToImage(texture);
  }
  const currentCareer: Item & any = actor.currentCareer;
  const careerData: ItemData & any = currentCareer;
  const careerDetail: any = careerData?.system;

  const skills = new LabelledValues(
    docBuilder.getGenerateType(),
    0,
    0,
    actor.itemCategories.skill
      .map((item) => {
        return {
          label: `${item.name} (${i18nLocalize(item.characteristic.abrev)})`,
          value: item.system.total.value,
        };
      })
      .sort((a, b) => a.label.localeCompare(b.label)),
    undefined,
    false,
    { name: 'skills' }
  );

  const talentsByName: { [name: string]: { count: number; test: string } } = {};

  actor.itemCategories.talent.forEach((item) => {
    const name = item.name;
    if (talentsByName[name] == null) {
      talentsByName[name] = { count: 1, test: item.system.tests.value };
    } else {
      talentsByName[name].count++;
    }
  });

  const talents = new LabelledValues(
    docBuilder.getGenerateType(),
    0,
    0,
    Object.entries(talentsByName)
      .map(([name, value]) => {
        return {
          label: value.test.length > 0 ? `${name} : ${value.test}` : name,
          value: value.count,
        };
      })
      .sort((a, b) => a.label.localeCompare(b.label)),
    1,
    true
  );

  const traits = new Texts(
    docBuilder.getGenerateType(),
    0,
    0,
    actor.itemCategories.trait
      .map((item) => {
        return item.name;
      })
      .sort((a, b) => a.localeCompare(b)),
    4
  );

  const weaponsMelee = new Texts(
    docBuilder.getGenerateType(),
    0,
    0,
    Util.getActorItems(actor, 'weapon')
      .filter((w) => w.isMelee)
      .map((item) => {
        return `${item.name} : ${item.WeaponGroup}, ${item.Reach}, ${
          item.system.damage.meleeValue
        } (${item.mountDamage}), ${item.OriginalQualities.concat(
          item.OriginalFlaws
        ).join(', ')}`;
      })
      .sort((a, b) => a.localeCompare(b)),
    1,
    true
  );

  const weaponsRanged = new Texts(
    docBuilder.getGenerateType(),
    0,
    0,
    Util.getActorItems(actor, 'weapon')
      .filter((w) => w.isRanged)
      .map((item) => {
        return `${item.name} : ${item.WeaponGroup}, ${
          item.system.range.value
        } (${item.Range}), ${item.system.damage.rangedValue} (${
          item.Damage
        }), ${item.OriginalQualities.concat(item.OriginalFlaws).join(', ')}`;
      })
      .sort((a, b) => a.localeCompare(b)),
    1,
    true
  );

  const ammunitions = new Texts(
    docBuilder.getGenerateType(),
    0,
    0,
    Util.getActorItems(actor, 'ammunition')
      .map((item) => {
        return `${item.system.quantity.value} ${item.name} : ${
          item.system.range.value.length > 0
            ? item.system.range.value
            : 'As Weapon'
        }, ${
          item.system.damage.value.length > 0 ? item.system.damage.value : '+0'
        }, ${item.OriginalQualities.concat(item.OriginalFlaws).join(', ')}`;
      })
      .sort((a, b) => a.localeCompare(b)),
    2,
    true
  );

  const armourLocation: string[] = [];
  const armourLabels: { [key: string]: string[] } = {};
  for (const armour of Util.getActorItems(actor, 'armour')) {
    const maxAp = armour.system.maxAP;
    for (const key of Object.keys(maxAp)) {
      if (maxAp[key] > 0) {
        if (!armourLocation.includes(key)) {
          armourLocation.push(key);
        }
        if (armourLabels[key] == null) {
          armourLabels[key] = [];
        }
        armourLabels[key].push(
          `${armour.name} ${maxAp[key]} ${armour.OriginalQualities.concat(
            armour.OriginalFlaws
          ).join(' ')}`
        );
      }
    }
  }

  const armours = new Texts(
    docBuilder.getGenerateType(),
    0,
    0,
    armourLocation.map((al) => {
      return `${actorStatus?.armour[al]?.label} : ${armourLabels[al]?.join(
        ', '
      )}`;
    }),
    1,
    true
  );

  const petty = new Texts(
    docBuilder.getGenerateType(),
    0,
    0,
    actor.itemCategories.spell
      .filter((s) => s.lore.value === 'petty')
      .map((s) => {
        return `${s.name} : ${s.cn.value}, ${s.Range}, ${s.Target}, ${s.Duration}`;
      }),
    2,
    true
  );

  const spell = new Texts(
    docBuilder.getGenerateType(),
    0,
    0,
    actor.itemCategories.spell
      .filter((s) => s.lore.value !== 'petty')
      .map((s) => {
        return `${s.name} : ${s.cn.value}, ${s.Range}, ${s.Target}, ${s.Duration}, ${s.ingredientList.length}`;
      }),
    2,
    true
  );

  const blessing = new Texts(
    docBuilder.getGenerateType(),
    0,
    0,
    actor.itemCategories.prayer
      .filter((s) => s.prayerType.value === 'blessing')
      .map((s) => {
        return `${s.name} : ${s.Range}, ${s.Target}, ${s.Duration}`;
      }),
    2,
    true
  );

  const miracle = new Texts(
    docBuilder.getGenerateType(),
    0,
    0,
    actor.itemCategories.prayer
      .filter((s) => s.prayerType.value !== 'blessing')
      .map((s) => {
        return `${s.name} : ${s.Range}, ${s.Target}, ${s.Duration}`;
      }),
    2,
    true
  );

  const allMoney = Util.getActorItems(actor, 'money');
  const moneyNames: string[] = [];
  const moneyByName: { [name: string]: number } = {};
  for (const money of allMoney) {
    if (!moneyNames.includes(money.name)) {
      moneyNames.push(money.name);
    }
    if (moneyByName[money.name] == null) {
      moneyByName[money.name] = 0;
    }
    moneyByName[money.name] = moneyByName[money.name] + money.quantity.value;
  }

  const trappingsHeader = new Texts(
    docBuilder.getGenerateType(),
    0,
    0,
    [
      `${i18nLocalize('Trappings')} : ${i18nLocalize('Money')} : ${moneyNames
        .map((m) => {
          return `${m} : ${moneyByName[m]}`;
        })
        .join(', ')}`,
    ],
    1,
    true
  );

  const trappings = new Texts(
    docBuilder.getGenerateType(),
    0,
    0,
    Util.getAllActorItems(actor, ['container', 'trapping'])
      .map((t) => {
        const location = t.location.value;
        let prefix = '';
        if (location != null && location !== 0) {
          prefix = `${actor.getEmbeddedDocument('Item', location).name} : `;
        }
        const qteLabel = t.quantity.value > 1 ? `${t.quantity.value} ` : '';
        return `${prefix}${qteLabel}${t.name}`;
      })
      .sort((a, b) => a.localeCompare(b)),
    4,
    true
  );

  const critical = new Texts(
    docBuilder.getGenerateType(),
    0,
    0,
    actor.itemCategories.critical.map((i) => {
      return i.name;
    }),
    3
  );

  const disease = new Texts(
    docBuilder.getGenerateType(),
    0,
    0,
    actor.itemCategories.disease.map((i) => {
      return i.name;
    }),
    3
  );

  const injury = new Texts(
    docBuilder.getGenerateType(),
    0,
    0,
    actor.itemCategories.injury.map((i) => {
      return i.name;
    }),
    3
  );

  const mutationP = new Texts(
    docBuilder.getGenerateType(),
    0,
    0,
    actor.itemCategories.mutation
      .filter((i) => i.mutationType.value === 'physical')
      .map((i) => {
        return i.name;
      }),
    3
  );

  const mutationM = new Texts(
    docBuilder.getGenerateType(),
    0,
    0,
    actor.itemCategories.mutation
      .filter((i) => i.mutationType.value === 'mental')
      .map((i) => {
        return i.name;
      }),
    3
  );

  const psychology = new Texts(
    docBuilder.getGenerateType(),
    0,
    0,
    actor.itemCategories.psychology.map((i) => {
      return i.name;
    }),
    3
  );

  const labelledRowHeight = docBuilder.getLabelledRowHeight();

  const imageWidth = 25 * docBuilder.getImageScale();
  const imageY = labelledRowHeight + MARGINS.top + 2;
  const actorImageElementPdf =
    actorImageData != null
      ? new Image(
          docBuilder.getGenerateType(),
          0,
          imageY,
          imageWidth,
          imageWidth,
          actorImageData,
          {
            isHtml: false,
          }
        )
      : new Box(
          docBuilder.getGenerateType(),
          0,
          imageY,
          imageWidth,
          imageWidth,
          {
            isHtml: false,
          }
        );
  const actorImageElementHtml =
    actorImageData != null
      ? new Image(
          docBuilder.getGenerateType(),
          0,
          imageY,
          imageWidth,
          imageWidth,
          actorImageData,
          {
            isPdf: false,
          }
        )
      : new Box(
          docBuilder.getGenerateType(),
          0,
          imageY,
          imageWidth,
          imageWidth,
          {
            isPdf: false,
          }
        );

  docBuilder.build([
    actorImageElementPdf,
    new Column(
      docBuilder.getGenerateType(),
      0,
      0,
      [
        new Row(docBuilder.getGenerateType(), 0, 0, [
          new LabelledText(
            docBuilder.getGenerateType(),
            0,
            0,
            'Name',
            `${actor.name}`
          ),
          new LabelledText(
            docBuilder.getGenerateType(),
            0,
            0,
            'Species',
            `${actorDetails?.species?.value}`
          ),
          new LabelledText(
            docBuilder.getGenerateType(),
            0,
            0,
            'Gender',
            `${actorDetails?.gender?.value}`
          ),
        ]),
        new Row(
          docBuilder.getGenerateType(),
          imageWidth + MARGINS.left + 1,
          0,
          [
            new LabelledText(
              docBuilder.getGenerateType(),
              0,
              0,
              'Class',
              `${careerDetail?.class?.value}`
            ),
            new LabelledText(
              docBuilder.getGenerateType(),
              0,
              0,
              'Career Group',
              `${careerDetail?.careergroup?.value}`
            ),
            new LabelledText(
              docBuilder.getGenerateType(),
              0,
              0,
              'Career',
              `${currentCareer?.name}`
            ),
          ],
          undefined,
          undefined,
          undefined,
          { isHtml: false }
        ),
        new Row(
          docBuilder.getGenerateType(),
          imageWidth + MARGINS.left + 1,
          0,
          [
            new LabelledText(
              docBuilder.getGenerateType(),
              0,
              0,
              'Status',
              `${actorDetails?.status?.value}`
            ),
            new LabelledText(
              docBuilder.getGenerateType(),
              0,
              0,
              'Age',
              `${actorDetails?.age?.value}`
            ),
            new LabelledText(
              docBuilder.getGenerateType(),
              0,
              0,
              'Height',
              `${actorDetails?.height?.value}`
            ),
            new LabelledText(
              docBuilder.getGenerateType(),
              0,
              0,
              'Weight',
              `${actorDetails?.weight?.value}`
            ),
            new LabelledText(
              docBuilder.getGenerateType(),
              0,
              0,
              'Hair Colour',
              `${actorDetails?.haircolour?.value}`
            ),
          ],
          undefined,
          undefined,
          undefined,
          { isHtml: false }
        ),
        new Row(
          docBuilder.getGenerateType(),
          imageWidth + MARGINS.left + 1,
          0,
          [
            new LabelledText(
              docBuilder.getGenerateType(),
              0,
              0,
              'Eye Colour',
              `${actorDetails?.eyecolour?.value}`
            ),
            new LabelledText(
              docBuilder.getGenerateType(),
              0,
              0,
              'Distinguishing Mark',
              `${actorDetails?.distinguishingmark?.value}`
            ),
            new LabelledText(
              docBuilder.getGenerateType(),
              0,
              0,
              'Star Sign',
              `${actorDetails?.starsign?.value}`
            ),
          ],
          undefined,
          undefined,
          undefined,
          { isHtml: false }
        ),
        new Row(
          docBuilder.getGenerateType(),
          0,
          0,
          [
            actorImageElementHtml,
            new Column(docBuilder.getGenerateType(), 0, 0, [
              new Row(
                docBuilder.getGenerateType(),
                imageWidth + MARGINS.left + 1,
                0,
                [
                  new LabelledText(
                    docBuilder.getGenerateType(),
                    0,
                    0,
                    'Class',
                    `${careerDetail?.class?.value}`
                  ),
                  new LabelledText(
                    docBuilder.getGenerateType(),
                    0,
                    0,
                    'Career Group',
                    `${careerDetail?.careergroup?.value}`
                  ),
                  new LabelledText(
                    docBuilder.getGenerateType(),
                    0,
                    0,
                    'Career',
                    `${currentCareer?.name}`
                  ),
                ]
              ),
              new Row(
                docBuilder.getGenerateType(),
                imageWidth + MARGINS.left + 1,
                0,
                [
                  new LabelledText(
                    docBuilder.getGenerateType(),
                    0,
                    0,
                    'Status',
                    `${actorDetails?.status?.value}`
                  ),
                  new LabelledText(
                    docBuilder.getGenerateType(),
                    0,
                    0,
                    'Age',
                    `${actorDetails?.age?.value}`
                  ),
                  new LabelledText(
                    docBuilder.getGenerateType(),
                    0,
                    0,
                    'Height',
                    `${actorDetails?.height?.value}`
                  ),
                  new LabelledText(
                    docBuilder.getGenerateType(),
                    0,
                    0,
                    'Weight',
                    `${actorDetails?.weight?.value}`
                  ),
                  new LabelledText(
                    docBuilder.getGenerateType(),
                    0,
                    0,
                    'Hair Colour',
                    `${actorDetails?.haircolour?.value}`
                  ),
                ]
              ),
              new Row(
                docBuilder.getGenerateType(),
                imageWidth + MARGINS.left + 1,
                0,
                [
                  new LabelledText(
                    docBuilder.getGenerateType(),
                    0,
                    0,
                    'Eye Colour',
                    `${actorDetails?.eyecolour?.value}`
                  ),
                  new LabelledText(
                    docBuilder.getGenerateType(),
                    0,
                    0,
                    'Distinguishing Mark',
                    `${actorDetails?.distinguishingmark?.value}`
                  ),
                  new LabelledText(
                    docBuilder.getGenerateType(),
                    0,
                    0,
                    'Star Sign',
                    `${actorDetails?.starsign?.value}`
                  ),
                ]
              ),
            ]),
          ],
          undefined,
          undefined,
          undefined,
          { isPdf: false }
        ),
        Blank.heightBlank(docBuilder.getGenerateType(), 2),
        new Row(docBuilder.getGenerateType(), 0, 0, [
          new LabelledText(
            docBuilder.getGenerateType(),
            0,
            0,
            'CHARAbbrev.WS',
            `${actorCharacs?.ws?.value}`
          ),
          new LabelledText(
            docBuilder.getGenerateType(),
            0,
            0,
            'CHARAbbrev.BS',
            `${actorCharacs?.bs?.value}`
          ),
          new LabelledText(
            docBuilder.getGenerateType(),
            0,
            0,
            'CHARAbbrev.S',
            `${actorCharacs?.s?.value}`
          ),
          new LabelledText(
            docBuilder.getGenerateType(),
            0,
            0,
            'CHARAbbrev.T',
            `${actorCharacs?.t?.value}`
          ),
          new LabelledText(
            docBuilder.getGenerateType(),
            0,
            0,
            'CHARAbbrev.I',
            `${actorCharacs?.i?.value}`
          ),
          new LabelledText(
            docBuilder.getGenerateType(),
            0,
            0,
            'CHARAbbrev.Ag',
            `${actorCharacs?.ag?.value}`
          ),
          new LabelledText(
            docBuilder.getGenerateType(),
            0,
            0,
            'CHARAbbrev.Dex',
            `${actorCharacs?.dex?.value}`
          ),
          new LabelledText(
            docBuilder.getGenerateType(),
            0,
            0,
            'CHARAbbrev.Int',
            `${actorCharacs?.int?.value}`
          ),
          new LabelledText(
            docBuilder.getGenerateType(),
            0,
            0,
            'CHARAbbrev.WP',
            `${actorCharacs?.wp?.value}`
          ),
          new LabelledText(
            docBuilder.getGenerateType(),
            0,
            0,
            'CHARAbbrev.Fel',
            `${actorCharacs?.fel?.value}`
          ),
        ]),
        new Row(docBuilder.getGenerateType(), 0, 0, [
          new LabelledText(
            docBuilder.getGenerateType(),
            0,
            0,
            'Move',
            `${actorDetails?.move?.value}`
          ),
          new LabelledText(
            docBuilder.getGenerateType(),
            0,
            0,
            'Walk',
            `${actorDetails?.move?.walk}`
          ),
          new LabelledText(
            docBuilder.getGenerateType(),
            0,
            0,
            'Run',
            `${actorDetails?.move?.run}`
          ),
          new LabelledText(
            docBuilder.getGenerateType(),
            0,
            0,
            'Fortune',
            `${actorStatus?.fortune?.value}`
          ),
          new LabelledText(
            docBuilder.getGenerateType(),
            0,
            0,
            'Fate',
            `${actorStatus?.fate?.value}`
          ),
          new LabelledText(
            docBuilder.getGenerateType(),
            0,
            0,
            'Resolve',
            `${actorStatus?.resolve?.value}`
          ),
          new LabelledText(
            docBuilder.getGenerateType(),
            0,
            0,
            'Resilience',
            `${actorStatus?.resilience?.value}`
          ),
          new LabelledText(
            docBuilder.getGenerateType(),
            0,
            0,
            'Wounds',
            `${actorStatus?.wounds?.value}/${actorStatus?.wounds?.max}`
          ),
        ]),
        new Separator(docBuilder.getGenerateType(), 0, 0),
        new Text(docBuilder.getGenerateType(), 0, 0, 'Skills'),
        skills,
        new Separator(docBuilder.getGenerateType(), 0, 0),
        new Text(
          docBuilder.getGenerateType(),
          0,
          0,
          `${i18nLocalize('Talents')} : ${i18nLocalize('Tests')}`
        ),
        talents,
        traits.contextElements.length > 0
          ? new Separator(docBuilder.getGenerateType(), 0, 0)
          : Blank.heightBlank(docBuilder.getGenerateType(), 0),
        traits.contextElements.length > 0
          ? new Text(docBuilder.getGenerateType(), 0, 0, 'Traits')
          : Blank.heightBlank(docBuilder.getGenerateType(), 0),
        traits,
        weaponsMelee.contextElements.length > 0
          ? new Separator(docBuilder.getGenerateType(), 0, 0)
          : Blank.heightBlank(docBuilder.getGenerateType(), 0),
        weaponsMelee.contextElements.length > 0
          ? new Text(
              docBuilder.getGenerateType(),
              0,
              0,
              `${i18nLocalize('SHEET.MeleeWeaponHeader')} : ${i18nLocalize(
                'Weapon Group'
              )}, ${i18nLocalize('Reach')}, ${i18nLocalize(
                'Damage'
              )}, ${i18nLocalize('Qualities')}, ${i18nLocalize('Flaws')}`
            )
          : Blank.heightBlank(docBuilder.getGenerateType(), 0),
        weaponsMelee,
        weaponsRanged.contextElements.length > 0
          ? new Separator(docBuilder.getGenerateType(), 0, 0)
          : Blank.heightBlank(docBuilder.getGenerateType(), 0),
        weaponsRanged.contextElements.length > 0
          ? new Text(
              docBuilder.getGenerateType(),
              0,
              0,
              `${i18nLocalize('SHEET.RangedWeaponHeader')} : ${i18nLocalize(
                'Weapon Group'
              )}, ${i18nLocalize('Range')}, ${i18nLocalize(
                'Damage'
              )}, ${i18nLocalize('Qualities')}, ${i18nLocalize('Flaws')}`
            )
          : Blank.heightBlank(docBuilder.getGenerateType(), 0),
        weaponsRanged,
        ammunitions.contextElements.length > 0
          ? new Separator(docBuilder.getGenerateType(), 0, 0)
          : Blank.heightBlank(docBuilder.getGenerateType(), 0),
        ammunitions.contextElements.length > 0
          ? new Text(
              docBuilder.getGenerateType(),
              0,
              0,
              `${i18nLocalize('Ammunition')} : ${i18nLocalize(
                'Range'
              )}, ${i18nLocalize('Damage')}, ${i18nLocalize(
                'Qualities'
              )}, ${i18nLocalize('Flaws')}`
            )
          : Blank.heightBlank(docBuilder.getGenerateType(), 0),
        ammunitions,
        armours.contextElements.length > 0
          ? new Separator(docBuilder.getGenerateType(), 0, 0)
          : Blank.heightBlank(docBuilder.getGenerateType(), 0),
        armours.contextElements.length > 0
          ? new Text(docBuilder.getGenerateType(), 0, 0, 'Armour')
          : Blank.heightBlank(docBuilder.getGenerateType(), 0),
        armours,
        petty.contextElements.length > 0
          ? new Separator(docBuilder.getGenerateType(), 0, 0)
          : Blank.heightBlank(docBuilder.getGenerateType(), 0),
        petty.contextElements.length > 0
          ? new Text(
              docBuilder.getGenerateType(),
              0,
              0,
              `${i18nLocalize('SHEET.PettySpell')} : ${i18nLocalize(
                'Casting Number'
              )}, ${i18nLocalize('Range')}, ${i18nLocalize(
                'Target'
              )}, ${i18nLocalize('Duration')}`
            )
          : Blank.heightBlank(docBuilder.getGenerateType(), 0),
        petty,
        spell.contextElements.length > 0
          ? new Separator(docBuilder.getGenerateType(), 0, 0)
          : Blank.heightBlank(docBuilder.getGenerateType(), 0),
        spell.contextElements.length > 0
          ? new Text(
              docBuilder.getGenerateType(),
              0,
              0,
              `${i18nLocalize('SHEET.LoreSpell')} : ${i18nLocalize(
                'Casting Number'
              )}, ${i18nLocalize('Range')}, ${i18nLocalize(
                'Target'
              )}, ${i18nLocalize('Duration')}, ${i18nLocalize(
                'WFRP4E.TrappingType.Ingredients'
              )}`
            )
          : Blank.heightBlank(docBuilder.getGenerateType(), 0),
        spell,
        blessing.contextElements.length > 0
          ? new Separator(docBuilder.getGenerateType(), 0, 0)
          : Blank.heightBlank(docBuilder.getGenerateType(), 0),
        blessing.contextElements.length > 0
          ? new Text(
              docBuilder.getGenerateType(),
              0,
              0,
              `${i18nLocalize('Blessing')} : ${i18nLocalize(
                'Range'
              )}, ${i18nLocalize('Target')}, ${i18nLocalize('Duration')}`
            )
          : Blank.heightBlank(docBuilder.getGenerateType(), 0),
        blessing,
        miracle.contextElements.length > 0
          ? new Separator(docBuilder.getGenerateType(), 0, 0)
          : Blank.heightBlank(docBuilder.getGenerateType(), 0),
        miracle.contextElements.length > 0
          ? new Text(
              docBuilder.getGenerateType(),
              0,
              0,
              `${i18nLocalize('Miracle')} : ${i18nLocalize(
                'Range'
              )}, ${i18nLocalize('Target')}, ${i18nLocalize('Duration')}`
            )
          : Blank.heightBlank(docBuilder.getGenerateType(), 0),
        miracle,
        new Separator(docBuilder.getGenerateType(), 0, 0),
        trappingsHeader,
        trappings,
        psychology.contextElements.length > 0
          ? new Separator(docBuilder.getGenerateType(), 0, 0)
          : Blank.heightBlank(docBuilder.getGenerateType(), 0),
        psychology.contextElements.length > 0
          ? new Text(docBuilder.getGenerateType(), 0, 0, 'Psychology')
          : Blank.heightBlank(docBuilder.getGenerateType(), 0),
        psychology,
        critical.contextElements.length > 0
          ? new Separator(docBuilder.getGenerateType(), 0, 0)
          : Blank.heightBlank(docBuilder.getGenerateType(), 0),
        critical.contextElements.length > 0
          ? new Text(docBuilder.getGenerateType(), 0, 0, 'Criticals')
          : Blank.heightBlank(docBuilder.getGenerateType(), 0),
        critical,
        disease.contextElements.length > 0
          ? new Separator(docBuilder.getGenerateType(), 0, 0)
          : Blank.heightBlank(docBuilder.getGenerateType(), 0),
        disease.contextElements.length > 0
          ? new Text(docBuilder.getGenerateType(), 0, 0, 'Diseases')
          : Blank.heightBlank(docBuilder.getGenerateType(), 0),
        disease,
        injury.contextElements.length > 0
          ? new Separator(docBuilder.getGenerateType(), 0, 0)
          : Blank.heightBlank(docBuilder.getGenerateType(), 0),
        injury.contextElements.length > 0
          ? new Text(docBuilder.getGenerateType(), 0, 0, 'Injuries')
          : Blank.heightBlank(docBuilder.getGenerateType(), 0),
        injury,
        mutationP.contextElements.length > 0
          ? new Separator(docBuilder.getGenerateType(), 0, 0)
          : Blank.heightBlank(docBuilder.getGenerateType(), 0),
        mutationP.contextElements.length > 0
          ? new Text(
              docBuilder.getGenerateType(),
              0,
              0,
              `${i18nLocalize('Mutations')} (${i18nLocalize('Physical')})`
            )
          : Blank.heightBlank(docBuilder.getGenerateType(), 0),
        mutationP,
        mutationM.contextElements.length > 0
          ? new Separator(docBuilder.getGenerateType(), 0, 0)
          : Blank.heightBlank(docBuilder.getGenerateType(), 0),
        mutationM.contextElements.length > 0
          ? new Text(
              docBuilder.getGenerateType(),
              0,
              0,
              `${i18nLocalize('Mutations')} (${i18nLocalize('Mental')})`
            )
          : Blank.heightBlank(docBuilder.getGenerateType(), 0),
        mutationM,
      ],
      {
        name: 'main-column',
      }
    ),
  ]);
  docBuilder.save(`${actor.name}`);
}
