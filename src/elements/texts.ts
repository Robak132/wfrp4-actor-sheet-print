import { Row } from './row';
import { Column } from './column';
import jsPDF from 'jspdf';
import { MARGINS } from '../constants';
import { Text } from './text';
import { MultilineText } from './multiline-text';
import { IContext } from './context';
import { AbstractElement } from './abstract-element';
import { GenerateTypeEnum } from './generate-type.enum';

export class Texts extends Row {
  public texts: string[];
  public nbrOfCol: number;

  constructor(
    globalType: GenerateTypeEnum,
    x: number,
    y: number,
    texts: string[],
    nbrOfCol?: number,
    multiline = false,
    context: Partial<IContext> = AbstractElement.DEFAULT_CONTEXT
  ) {
    super(globalType, x, y, [], undefined, undefined, undefined, context);
    this.texts = texts;
    this.nbrOfCol = nbrOfCol ?? 4;
    if (this.nbrOfCol > 4) {
      this.nbrOfCol = 4;
    }
    let currentIndex = 0;
    if (texts.length > 0) {
      if (this.nbrOfCol > 1) {
        const nbrPerCol = Math.floor(texts.length / this.nbrOfCol);
        const rest = texts.length - nbrPerCol * this.nbrOfCol;
        const nbrPerCols = [
          rest > 0 ? nbrPerCol + 1 : nbrPerCol,
          rest > 1 ? nbrPerCol + 1 : nbrPerCol,
          rest > 2 ? nbrPerCol + 1 : nbrPerCol,
          rest > 3 ? nbrPerCol + 1 : nbrPerCol,
        ];
        for (let i = 0; i < this.nbrOfCol; i++) {
          this.contextElements[i] = new Column(this.globalType, 0, 0, []);
        }
        for (let i = 0; i < texts.length; i++) {
          if (i < nbrPerCols[0]) {
            currentIndex = 0;
          } else if (i < nbrPerCols[0] + nbrPerCols[1]) {
            currentIndex = 1;
          } else if (i < nbrPerCols[0] + nbrPerCols[1] + nbrPerCols[2]) {
            currentIndex = 2;
          } else {
            currentIndex = 3;
          }
          (<Column>this.contextElements[currentIndex]).contextElements.push(
            new Row(this.globalType, 0, 0, [
              multiline
                ? new MultilineText(this.globalType, 0, 0, texts[i])
                : new Text(this.globalType, 0, 0, texts[i]),
            ])
          );
        }
      } else {
        this.contextElements.push(
          new Column(
            this.globalType,
            0,
            0,
            texts.map(
              (text) =>
                new Row(this.globalType, 0, 0, [
                  multiline
                    ? new MultilineText(this.globalType, 0, 0, text)
                    : new Text(this.globalType, 0, 0, text),
                ])
            )
          )
        );
      }
    }
  }

  public prepareRender(doc: jsPDF, maxWidth?: number): jsPDF {
    const pageWidth = doc.internal.pageSize.width;
    const rowWidth = pageWidth - this.x - MARGINS.right;
    for (const column of this.pdfElements) {
      for (const labelledValue of (<Column>column).pdfElements) {
        labelledValue.maxWidth = rowWidth / this.nbrOfCol;
      }
    }
    return super.prepareRender(doc, maxWidth);
  }

  public render(doc: jsPDF, _maxWidth?: number): jsPDF {
    return doc;
  }
}
