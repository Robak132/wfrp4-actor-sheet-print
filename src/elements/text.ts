import { AbstractElement } from './abstract-element';
import jsPDF, { TextOptionsLight } from 'jspdf';
import { HTML_TEXT_SIZE, i18nLocalize, TEXT_SIZE } from '../constants';
import { IContext } from './context';
import { GenerateTypeEnum } from './generate-type.enum';

export class Text extends AbstractElement {
  public text: string;
  public textOptions?: TextOptionsLight;

  constructor(
    globalType: GenerateTypeEnum,
    x: number,
    y: number,
    text: string,
    textOptions?: TextOptionsLight,
    context: Partial<IContext> = AbstractElement.DEFAULT_CONTEXT
  ) {
    super(globalType, x, y, textOptions?.maxWidth, context);
    this.text = text;
    this.textOptions = textOptions;
  }

  public prepareRender(doc: jsPDF, maxWidth?: number): jsPDF {
    this.updateMaxWidth(maxWidth);
    return doc;
  }

  public render(doc: jsPDF, _maxWidth?: number): jsPDF {
    doc.setFontSize(TEXT_SIZE);
    let finalText: string[] = [i18nLocalize(this.text)];
    if (this.maxWidth != null) {
      finalText = doc.splitTextToSize(finalText[0], this.maxWidth);
    }
    if (finalText.length > 1) {
      finalText[0] = finalText[0].replace(/(.){3}$/, '...');
    }
    const yText = this.y + this.getHeightFromPx(doc, TEXT_SIZE);
    doc.text(finalText[0], this.x, yText, this.textOptions);
    return doc;
  }

  public renderHtml(
    doc: Document,
    parent: HTMLElement,
    cssRules: string[],
    sheet: HTMLStyleElement
  ): Document {
    const text = doc.createElement('p');
    const css = `text-${TEXT_SIZE}`;
    text.classList.add(`ellipsis`);
    text.classList.add(css);
    text.classList.add(this.context.name);
    text.innerHTML = i18nLocalize(i18nLocalize(this.text));
    if (!cssRules.includes(css)) {
      cssRules.push(css);
      sheet.innerHTML += ` .${css} { font-size: ${HTML_TEXT_SIZE}rem }`;
      sheet.innerHTML += ` .labelled-text > .${css} { font-size: ${HTML_TEXT_SIZE}rem; margin: 0; }`;
    }
    parent.append(text);
    return doc;
  }

  protected updateMaxWidth(maxWidth?: number) {
    if (maxWidth != null && maxWidth > 0) {
      this.maxWidth = maxWidth;
      const options: TextOptionsLight = this.textOptions ?? {};
      options.maxWidth = this.maxWidth;
      this.textOptions = options;
    }
  }

  public getHeight(doc): number {
    return this.getHeightFromPx(doc, TEXT_SIZE);
  }

  public getCheckNewPageHeight(doc?: jsPDF): number {
    return this.getHeight(doc);
  }

  public getElements(): AbstractElement[] {
    return [this];
  }
}
